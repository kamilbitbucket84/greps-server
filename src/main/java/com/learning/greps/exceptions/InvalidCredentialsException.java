package com.learning.greps.exceptions;

/**
 * Created by kamil on 29/06/2017.
 */
public class InvalidCredentialsException extends Exception {

    private static final String MESSAGE = "Invalid username or password";

    public InvalidCredentialsException() {
        super(MESSAGE);
    }
}
