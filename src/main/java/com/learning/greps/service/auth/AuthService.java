package com.learning.greps.service.auth;

import com.learning.greps.domain.request.UserBody;
import com.learning.greps.domain.response.AuthResponseEntity;
import rx.Observable;

/**
 * Created by kamil on 14/06/2017.
 */

public interface AuthService {

    Observable<AuthResponseEntity> registerUser(UserBody userBody);

    Observable<AuthResponseEntity> loginUser(String username, String password);
}
