package com.learning.greps.service.auth;

import com.learning.greps.domain.request.UserBody;
import com.learning.greps.domain.response.AuthResponseEntity;
import com.learning.greps.domain.usecase.GetLoginUserUseCase;
import com.learning.greps.domain.usecase.PostRegisterUserUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

/**
 * Created by kamil on 14/06/2017.
 */

@Service

public class RestAuthService implements AuthService {

    @Autowired GetLoginUserUseCase getLoginUserUseCase;
    @Autowired PostRegisterUserUseCase postRegisterUserUseCase;

    @Override
    public Observable<AuthResponseEntity> registerUser(UserBody userBody) {
        return postRegisterUserUseCase.withUserBody(userBody).execute();
    }

    @Override
    public Observable<AuthResponseEntity> loginUser(String username, String password) {
        return getLoginUserUseCase.withCredentials(username, password).execute();
    }
}
