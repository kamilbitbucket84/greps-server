package com.learning.greps.service.auth;

import com.learning.greps.domain.database.UserDbEntity;

import java.io.UnsupportedEncodingException;

/**
 * Created by kamil on 19/06/2017.
 */

public interface Authenticator {

    String generateEncryptedPassword(String password);

    boolean checkUserPassword(String enteredPassword, String passwordFromDb);

    String generateJWToken(UserDbEntity userDbEntity) throws UnsupportedEncodingException;

    void addTokenToCache(String token);

    void removeTokenFromCache(String token);

    boolean isTokenValid(String token);
}
