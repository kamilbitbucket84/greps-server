package com.learning.greps.service.auth;

import com.learning.greps.domain.database.UserDbEntity;
import io.jsonwebtoken.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by kamil on 19/06/2017.
 */

public class GrepsAuthenticator implements Authenticator {

    private HashMap<String, Long> tokenMap = new HashMap<>();
    private BCryptPasswordEncoder encoder;
    private String passwordSalt;
    private String signKey;
    private String encoding;
    private String issuer;
    private long tokenCreationExpirationTime;
    private long tokenUsageExpirationTime;

    private GrepsAuthenticator(Builder builder) {
        encoder = builder.encoder;
        passwordSalt = builder.passwordSalt;
        signKey = builder.signKey;
        encoding = builder.encoding;
        issuer = builder.issuer;
        tokenCreationExpirationTime = builder.tokenCreationExpirationTime;
        tokenUsageExpirationTime = builder.tokenUsageExpirationTime;
    }

    @Override
    public String generateEncryptedPassword(String password) {
        return encoder.encode(password + passwordSalt);
    }

    @Override
    public boolean checkUserPassword(String enteredPassword, String passwordFromDb) {
        return encoder.matches(enteredPassword + passwordSalt, passwordFromDb);
    }

    @Override
    public String generateJWToken(UserDbEntity userDbEntity) throws UnsupportedEncodingException {
        JwtBuilder builder = Jwts.builder()
                .setIssuer(issuer)
                .setExpiration(new Date(tokenCreationExpirationTime))
                .signWith(SignatureAlgorithm.HS256, signKey.getBytes(encoding))
                .claim("id", userDbEntity.id)
                .claim("user_name", userDbEntity.username);

        return builder.compact();
    }

    @Override
    public void addTokenToCache(String token) {
        tokenMap.put(token, System.currentTimeMillis() + tokenUsageExpirationTime);
    }

    @Override
    public void removeTokenFromCache(String token) {
        tokenMap.remove(token);
    }

    @Override
    public boolean isTokenValid(String token) {
        try {
            Jws<Claims> jwt = Jwts.parser().setSigningKey(signKey.getBytes(encoding)).parseClaimsJws(token);
            if (tokenMap.containsKey(token)) {
                if (tokenMap.get(token) > System.currentTimeMillis()) {
                    addTokenToCache(token);
                    return true;
                } else {
                    removeTokenFromCache(token);
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception exc) {
            return false;
        }
    }

    public static final class Builder {
        private BCryptPasswordEncoder encoder;
        private String passwordSalt;
        private String signKey;
        private String encoding;
        private String issuer;
        private long tokenCreationExpirationTime;
        private long tokenUsageExpirationTime;

        public Builder() {
        }

        public Builder encoder(BCryptPasswordEncoder encoder) {
            this.encoder = encoder;
            return this;
        }

        public Builder passwordSalt(String passwordSalt) {
            this.passwordSalt = passwordSalt;
            return this;
        }

        public Builder signKey(String signKey) {
            this.signKey = signKey;
            return this;
        }

        public Builder encoding(String encoding) {
            this.encoding = encoding;
            return this;
        }

        public Builder issuer(String issuer) {
            this.issuer = issuer;
            return this;
        }

        public Builder tokenCreationExpirationTime(long tokenCreationExpirationTime) {
            this.tokenCreationExpirationTime = tokenCreationExpirationTime;
            return this;
        }

        public Builder tokenUsageExpirationTime(long tokenUsageExpirationTime) {
            this.tokenUsageExpirationTime = tokenUsageExpirationTime;
            return this;
        }

        public GrepsAuthenticator build() {
            return new GrepsAuthenticator(this);
        }
    }
}