package com.learning.greps.service.greps;

import com.learning.greps.domain.request.ConnectionBody;
import com.learning.greps.domain.request.MessageBody;
import com.learning.greps.domain.response.*;
import com.learning.greps.domain.usecase.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

/**
 * Created by kamil on 24/05/2017.
 */

@Service
public class RestGrepsService implements GrepsService {

    @Autowired private GetMyUsersUseCase getMyUsersUseCase;
    @Autowired private GetFoundUsersUseCase getFoundUsersUseCase;
    @Autowired private PostConnectUserUseCase postConnectUserUseCase;
    @Autowired private PostDisconnectUserUseCase postDisconnectUserUseCase;
    @Autowired private GetAllMessagesWithUserUseCase getAllMessagesWithUserUseCase;
    @Autowired private PostSendMessageUseCase postSendMessageUseCase;

    @Override
    public Observable<GetMyUsersResponseEntity> getMyUsers(Long userId) {
        return getMyUsersUseCase.withUserId(userId).execute();
    }

    @Override
    public Observable<GetFoundUsersResponseEntity> findUsers(String usernameQuery, Long userId) {
        return getFoundUsersUseCase.withQuery(usernameQuery, userId).execute();
    }

    @Override
    public Observable<PostConnectUserResponseEntity> connectUser(ConnectionBody connectionBody) {
        return postConnectUserUseCase.withConnectionBody(connectionBody).execute();
    }

    @Override
    public Observable<PostDisconnectUserResponseEntity> disconnectUser(ConnectionBody connectionBody) {
        return postDisconnectUserUseCase.withConnectionBody(connectionBody).execute();
    }

    @Override
    public Observable<GetMessagesWithUserResponseEntity> getMessagesWithUser(Long myId, Long contactId) {
        return getAllMessagesWithUserUseCase.withUsersId(myId, contactId).execute();
    }

    @Override
    public Observable<SendMessageResponseEntity> sendMessage(MessageBody messageBody) {
        return postSendMessageUseCase.withMessageBody(messageBody).execute();
    }
}