package com.learning.greps.service.greps;

import com.learning.greps.domain.request.ConnectionBody;
import com.learning.greps.domain.request.MessageBody;
import com.learning.greps.domain.response.*;
import rx.Observable;


/**
 * Created by kamil on 24/05/2017.
 */

public interface GrepsService {

    Observable<GetMyUsersResponseEntity> getMyUsers(Long userId);

    Observable<GetFoundUsersResponseEntity> findUsers(String usernameQuery, Long userId);

    Observable<PostConnectUserResponseEntity> connectUser(ConnectionBody connectionBody);

    Observable<PostDisconnectUserResponseEntity> disconnectUser(ConnectionBody connectionBody);

    Observable<GetMessagesWithUserResponseEntity> getMessagesWithUser(Long myId, Long contactId);

    Observable<SendMessageResponseEntity> sendMessage(MessageBody messageBody);
}
