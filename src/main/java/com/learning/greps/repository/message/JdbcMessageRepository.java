package com.learning.greps.repository.message;

import com.learning.greps.domain.database.MessageDbEntity;
import com.learning.greps.domain.mapper.MessageDbEntityRowMapper;
import com.learning.greps.domain.request.MessageBody;
import com.learning.greps.repository.SQLQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import rx.Observable;

import java.util.List;

/**
 * Created by kamil on 25/05/2017.
 */

@Repository
@Qualifier(MessageRepository.JDBC_POSTGRES)
public class JdbcMessageRepository implements MessageRepository {

    @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Observable<List<MessageDbEntity>> getAllMessagesWithUser(Long senderId, Long receiverId) {
        return Observable.fromCallable(() -> {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue(SQLQueries.PARAM_SENDER_ID, senderId);
            params.addValue(SQLQueries.PARAM_RECEIVER_ID, receiverId);
            MessageDbEntityRowMapper mapper = new MessageDbEntityRowMapper();

            List<MessageDbEntity> messageDbEntities
                    = namedParameterJdbcTemplate.query(SQLQueries.SELECT_ALL_MESSAGES_WITH_USER, params, mapper);

            return messageDbEntities;
        });
    }

    @Override
    public Observable<MessageDbEntity> sendMessage(MessageBody messageBody) {
        return Observable.fromCallable(() -> {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue(SQLQueries.PARAM_SENDER_ID, messageBody.getSenderId());
            params.addValue(SQLQueries.PARAM_RECEIVER_ID, messageBody.getReceiverId());
            params.addValue(SQLQueries.PARAM_MESSAGE, messageBody.getMessage());
            MessageDbEntityRowMapper mapper = new MessageDbEntityRowMapper();

            MessageDbEntity messageDbEntity
                    = namedParameterJdbcTemplate.queryForObject(SQLQueries.INSERT_SEND_MESSAGE, params, mapper);

            return messageDbEntity;
        });
    }
}
