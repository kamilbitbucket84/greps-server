package com.learning.greps.repository.message;

import com.learning.greps.domain.database.MessageDbEntity;
import com.learning.greps.domain.request.MessageBody;
import rx.Observable;

import java.util.List;

/**
 * Created by kamil on 25/05/2017.
 */

public interface MessageRepository {

    String MOCKED = "mocked";
    String JDBC_POSTGRES = "postgres";

    Observable<List<MessageDbEntity>> getAllMessagesWithUser(Long myId, Long contactId);

    Observable<MessageDbEntity> sendMessage(MessageBody messageBody);
}