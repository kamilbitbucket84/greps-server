package com.learning.greps.repository.message;

import com.learning.greps.domain.database.MessageDbEntity;
import com.learning.greps.domain.request.MessageBody;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 25/05/2017.
 */

@Repository
@Qualifier(MessageRepository.MOCKED)
public class MockedMessageRepository implements MessageRepository {

    @Override
    public Observable<List<MessageDbEntity>> getAllMessagesWithUser(Long senderId, Long receiverId) {
        return Observable.fromCallable(() -> {
            List<MessageDbEntity> messageDbEntities = new ArrayList<>();
            MessageDbEntity messageDbEntity = null;
            for (int i = 0; i < 30; i++) {
                if (i % 2 == 0) {
                    messageDbEntity = new MessageDbEntity();
                    messageDbEntity.idMessage = (long) i;
                    messageDbEntity.message = "Siema";
                    messageDbEntity.idMessageSender = senderId;
                    messageDbEntity.idMessageReceiver = receiverId;
                    messageDbEntity.timeStamp = System.currentTimeMillis();

                } else {
                    messageDbEntity = new MessageDbEntity();
                    messageDbEntity.idMessage = (long) i;
                    messageDbEntity.message = "No siemano, co tam jak tam";
                    messageDbEntity.idMessageSender = receiverId;
                    messageDbEntity.idMessageReceiver = senderId;
                    messageDbEntity.timeStamp = System.currentTimeMillis();
                }

                messageDbEntities.add(messageDbEntity);
            }

            return messageDbEntities;
        });
    }

    @Override
    public Observable<MessageDbEntity> sendMessage(MessageBody messageBody) {
        return Observable.fromCallable(() -> {
            MessageDbEntity messageDbEntity = new MessageDbEntity();
            messageDbEntity.idMessage = 1L;
            messageDbEntity.message = messageBody.getMessage();
            messageDbEntity.idMessageSender = messageBody.getSenderId();
            messageDbEntity.idMessageReceiver = messageBody.getReceiverId();
            messageDbEntity.timeStamp = System.currentTimeMillis();

            return messageDbEntity;
        });
    }
}
