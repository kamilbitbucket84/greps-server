package com.learning.greps.repository.user;

import com.learning.greps.domain.database.ConnectUserDbEntity;
import com.learning.greps.domain.database.FoundUserDbEntity;
import com.learning.greps.domain.database.MyUserDbEntity;
import com.learning.greps.domain.request.ConnectionBody;
import rx.Observable;

import java.util.List;


/**
 * Created by kamil on 24/05/2017.
 */

public interface UserRepository {

    String MOCKED = "mocked";
    String JDBC_POSTGRES = "postgres";

    Observable<ConnectUserDbEntity> connectUser(ConnectionBody connectionBody);

    Observable<ConnectUserDbEntity> disconnectUser(ConnectionBody disconnectUserBody);

    Observable<List<MyUserDbEntity>> getMyUsers(Long userId);

    Observable<List<FoundUserDbEntity>> findUsers(String usernameQuery, Long userId);
}
