package com.learning.greps.repository.user;

import com.learning.greps.domain.database.ConnectUserDbEntity;
import com.learning.greps.domain.database.FoundUserDbEntity;
import com.learning.greps.domain.database.MyUserDbEntity;
import com.learning.greps.domain.mapper.ConnectUserDbEntityRowMapper;
import com.learning.greps.domain.mapper.FoundUserDbEntityRowMapper;
import com.learning.greps.domain.mapper.MyUserDbEntityRowMapper;
import com.learning.greps.domain.request.ConnectionBody;
import com.learning.greps.repository.SQLQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import rx.Observable;

import java.util.List;

/**
 * Created by kamil on 24/05/2017.
 */

@Repository
@Qualifier(UserRepository.JDBC_POSTGRES)
public class JdbcUserRepository implements UserRepository {

    @Autowired private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired private JdbcTemplate jdbcTemplate;

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    @Override
    public Observable<ConnectUserDbEntity> connectUser(ConnectionBody connectionBody) {
        return Observable.fromCallable(() -> {
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue(SQLQueries.PARAM_SENDER_ID, connectionBody.getSenderId());
            parameterSource.addValue(SQLQueries.PARAM_RECEIVER_ID, connectionBody.getReceiverId());
            ConnectUserDbEntityRowMapper mapper = new ConnectUserDbEntityRowMapper();

            ConnectUserDbEntity connectUserDbEntity
                    = namedParameterJdbcTemplate.queryForObject(SQLQueries.INSERT_CONNECT_USER_SQL, parameterSource, mapper);

            return connectUserDbEntity;
        });
    }

    @Override
    public Observable<ConnectUserDbEntity> disconnectUser(ConnectionBody disconnectUserBody) {
        return Observable.fromCallable(() -> {
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue(SQLQueries.PARAM_SENDER_ID, disconnectUserBody.getSenderId());
            parameterSource.addValue(SQLQueries.PARAM_RECEIVER_ID, disconnectUserBody.getReceiverId());

            int result = namedParameterJdbcTemplate.update(SQLQueries.DELETE_DISCONNECT_USER_SQL, parameterSource);

            if (result == 1) {
                ConnectUserDbEntity connectUserDbEntity = new ConnectUserDbEntity();
                connectUserDbEntity.idSender = disconnectUserBody.getSenderId();
                connectUserDbEntity.idReceiver = disconnectUserBody.getReceiverId();
                return connectUserDbEntity;
            } else {
                return null;
            }
        });
    }

    @Override
    public Observable<List<MyUserDbEntity>> getMyUsers(Long userId) {
        return Observable.fromCallable(() -> {
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue(SQLQueries.PARAM_USER_ID, userId);
            MyUserDbEntityRowMapper mapper = new MyUserDbEntityRowMapper();

            List<MyUserDbEntity> myUserDbEntities =
                    namedParameterJdbcTemplate.query(SQLQueries.SELECT_MY_USERS_SQL, parameterSource, mapper);

            return myUserDbEntities;
        });
    }

    @Override
    public Observable<List<FoundUserDbEntity>> findUsers(String usernameQuery, Long userId) {
        return Observable.fromCallable(() -> {
            List<FoundUserDbEntity> foundUserDbEntities
                    = jdbcTemplate.query(SQLQueries.SELECT_FILTERED_USERS_SQL,
                    new Object[]{"%" + usernameQuery + "%", userId},
                    new FoundUserDbEntityRowMapper());

            return foundUserDbEntities;
        });
    }
}