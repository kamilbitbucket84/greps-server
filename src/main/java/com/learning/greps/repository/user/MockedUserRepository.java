package com.learning.greps.repository.user;

import com.learning.greps.domain.database.ConnectUserDbEntity;
import com.learning.greps.domain.database.FoundUserDbEntity;
import com.learning.greps.domain.database.MyUserDbEntity;
import com.learning.greps.domain.request.ConnectionBody;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import rx.Observable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 24/05/2017.
 */

@Repository
@Qualifier(UserRepository.MOCKED)
public class MockedUserRepository implements UserRepository {

    @Override
    public Observable<ConnectUserDbEntity> connectUser(ConnectionBody connectionBody) {
        return Observable.fromCallable(() -> {
            ConnectUserDbEntity connectUserDbEntity = new ConnectUserDbEntity();
            connectUserDbEntity.idConnection = 1L;
            connectUserDbEntity.idSender = connectionBody.getSenderId();
            connectUserDbEntity.idReceiver = connectionBody.getReceiverId();
            return connectUserDbEntity;
        });
    }

    @Override
    public Observable<ConnectUserDbEntity> disconnectUser(ConnectionBody disconnectUserBody) {
        return Observable.fromCallable(() -> {
            ConnectUserDbEntity connectUserDbEntity = new ConnectUserDbEntity();
            connectUserDbEntity.idConnection = 1L;
            connectUserDbEntity.idSender = disconnectUserBody.getSenderId();
            connectUserDbEntity.idReceiver = disconnectUserBody.getReceiverId();
            return connectUserDbEntity;
        });
    }

    @Override
    public Observable<List<MyUserDbEntity>> getMyUsers(Long userId) {
        return Observable.fromCallable(() -> {
            List<MyUserDbEntity> myUserDbEntities = new ArrayList<>();

            MyUserDbEntity myUserDbEntity;
            for (int i = 0; i < 10; i++) {
                myUserDbEntity = new MyUserDbEntity();
                myUserDbEntity.id = (long) i;
                myUserDbEntity.userName = "John" + i;
                myUserDbEntities.add(myUserDbEntity);
            }

            return myUserDbEntities;
        });
    }

    @Override
    public Observable<List<FoundUserDbEntity>> findUsers(String usernameQuery, Long userId) {
        return Observable.fromCallable(() -> {
            List<FoundUserDbEntity> foundUsers = new ArrayList<>();

            FoundUserDbEntity u1 = new FoundUserDbEntity();
            u1.id = 1L;
            u1.userName = "Brandon";
            u1.connected = true;
            foundUsers.add(u1);

            FoundUserDbEntity u2 = new FoundUserDbEntity();
            u2.id = 2L;
            u2.userName = "James";
            u2.connected = true;
            foundUsers.add(u2);

            FoundUserDbEntity u3 = new FoundUserDbEntity();
            u3.id = 3L;
            u3.userName = "Natasha";
            u3.connected = false;
            foundUsers.add(u3);

            return foundUsers;
        });
    }
}
