package com.learning.greps.repository;

/**
 * Created by kamil on 01/06/2017.
 */

public final class SQLQueries {

    private SQLQueries() {
    }

    public final static String PARAM_QUERY = "query";
    public final static String PARAM_USER_ID = "user_id";
    public final static String PARAM_SENDER_ID = "sender_id";
    public final static String PARAM_RECEIVER_ID = "receiver_id";
    public final static String PARAM_MESSAGE = "message";
    public final static String PARAM_USERNAME = "user_name";
    public final static String PARAM_USER_EMAIL = "user_email";
    public final static String PARAM_USER_PASSWORD = "user_password";

    public static final String SELECT_FILTERED_USERS_SQL = "select\n" +
            "\tfiltered.id_user,\n" +
            "\tfiltered.username,\n" +
            "\tcase when connected.receiver_id is null then false else true end as connected\n" +
            "from\n" +
            "\t(\n" +
            "\t\tselect\n" +
            "\t\t\tgreps.greps_user.id_user as id_user,\n" +
            "\t\t\tgreps.greps_user.user_name as username\n" +
            "\t\tfrom\n" +
            "\t\t\tgreps.greps_user\n" +
            "\t\twhere\n" +
            "\t\t\tupper(user_name) like upper(?)\n" +
            "\t) filtered left join (\n" +
            "\t\tselect\n" +
            "\t\t\tgreps.connected_users.id_receiver as receiver_id\n" +
            "\t\tfrom\n" +
            "\t\t\tgreps.connected_users\n" +
            "\t\twhere\n" +
            "\t\t\tgreps.connected_users.id_sender = ?\n" +
            "\t) connected \n" +
            "\ton filtered.id_user = connected.receiver_id";

    public static final String SELECT_MY_USERS_SQL = "select\n" +
            "        greps.connected_users.id_receiver as id_user,\n" +
            "        receiver.user_name\n" +
            "    from\n" +
            "        greps.connected_users\n" +
            "        join greps.greps_user receiver \ton receiver.id_user = greps.connected_users.id_receiver\n" +
            "    where\n" +
            "        greps.connected_users.id_sender = :user_id;";

    public static final String INSERT_CONNECT_USER_SQL =
            "insert into greps.connected_users(id_sender, id_receiver) values(:sender_id,:receiver_id) " +
                    "returning greps.connected_users.id_connection, id_sender, id_receiver;\n";

    public static final String DELETE_DISCONNECT_USER_SQL =
            "delete from greps.connected_users where id_sender = :sender_id and id_receiver = :receiver_id;";

    public static final String INSERT_SEND_MESSAGE =
            "insert into greps.message(id_message_sender, id_message_receiver, message) values(:sender_id, :receiver_id, :message)\n" +
                    "returning id_message, message, id_message_sender, id_message_receiver, message_timestamp;\n";

    public static final String SELECT_ALL_MESSAGES_WITH_USER = "select\n" +
            "\tgreps.message.id_message,\n" +
            "\tgreps.message.message,\n" +
            "\tgreps.message.id_message_sender,\n" +
            "\tsender.user_name as sender_name,\n" +
            "\tgreps.message.id_message_receiver,\n" +
            "\treceiver.user_name as receiver_name,\n" +
            "\tgreps.message.message_timestamp\n" +
            "from\n" +
            "\tgreps.message join greps.greps_user receiver on\n" +
            "\treceiver.id_user = greps.message.id_message_receiver join greps.greps_user sender on\n" +
            "\tsender.id_user = greps.message.id_message_sender\n" +
            "where\n" +
            "\tgreps.message.id_message_sender = :sender_id\n" +
            "\tand greps.message.id_message_receiver = :receiver_id\n" +
            "\tor greps.message.id_message_sender = :receiver_id\n" +
            "\tand greps.message.id_message_receiver = :sender_id;\n";

    public static final String INSERT_REGISTER_USER = "insert into greps.greps_user(user_name, email, password) " +
            "values(:user_name,:user_email, :user_password)\n" +
            "returning id_user, user_name, email, password\n";

    public static final String SELECT_USER_WITH_USERNAME = "select * from greps.greps_user \n" +
            "where user_name = :user_name\n";
}
