package com.learning.greps.repository.auth;

import com.learning.greps.domain.database.UserDbEntity;
import com.learning.greps.domain.mapper.UserDbEntityRowMapper;
import com.learning.greps.domain.request.UserBody;
import com.learning.greps.domain.response.AuthResponseEntity;
import com.learning.greps.exceptions.InvalidCredentialsException;
import com.learning.greps.repository.SQLQueries;
import com.learning.greps.service.auth.Authenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import rx.Observable;

/**
 * Created by kamil on 20/06/2017.
 */

@Repository
@Qualifier(AuthRepository.JDBC_POSTGRES)
public class JdbcAuthRepository implements AuthRepository {

    @Autowired private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired private DataSourceTransactionManager transactionManager;
    @Autowired private Authenticator authenticator;

    @Override
    public Observable<AuthResponseEntity> registerUser(UserBody userBody) {
        return Observable.fromCallable(() -> {
            TransactionDefinition definition = new DefaultTransactionDefinition();
            TransactionStatus transactionStatus = transactionManager.getTransaction(definition);
            AuthResponseEntity authResponseEntity = null;

            try {

                String generatedPassword = authenticator.generateEncryptedPassword(userBody.getPassword());
                UserDbEntity userDbEntity = insertNewUserInDatabase(userBody, generatedPassword);
                String jwt = authenticator.generateJWToken(userDbEntity);
                authenticator.addTokenToCache(jwt);

                transactionManager.commit(transactionStatus);
                authResponseEntity = new AuthResponseEntity(userDbEntity.id, userDbEntity.username, userDbEntity.email, jwt);
            } catch (Exception exc) {
                transactionManager.rollback(transactionStatus);
                throw exc;
            }

            return authResponseEntity;
        });
    }

    @Override
    public Observable<AuthResponseEntity> loginUser(String username, String password) {
        return Observable.fromCallable(() -> {
            TransactionDefinition definition = new DefaultTransactionDefinition();
            TransactionStatus transactionStatus = transactionManager.getTransaction(definition);
            AuthResponseEntity authResponseEntity = null;

            try {
                UserDbEntity userDbEntity = selectUserWithUserName(username);
                boolean checkPassword = authenticator.checkUserPassword(password, userDbEntity.password);
                if (!checkPassword) throw new InvalidCredentialsException();
                String jwt = authenticator.generateJWToken(userDbEntity);
                authenticator.addTokenToCache(jwt);

                transactionManager.commit(transactionStatus);
                authResponseEntity = new AuthResponseEntity(userDbEntity.id, userDbEntity.username, userDbEntity.email, jwt);
            } catch (EmptyResultDataAccessException exc) {
                transactionManager.rollback(transactionStatus);
                throw new InvalidCredentialsException();
            } catch (Exception exc) {
                transactionManager.rollback(transactionStatus);
                throw exc;
            }

            return authResponseEntity;
        });
    }

    private UserDbEntity insertNewUserInDatabase(UserBody userBody, String generatedPassword) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(SQLQueries.PARAM_USERNAME, userBody.getUsername());
        parameterSource.addValue(SQLQueries.PARAM_USER_EMAIL, userBody.getEmail());
        parameterSource.addValue(SQLQueries.PARAM_USER_PASSWORD, generatedPassword);
        UserDbEntityRowMapper mapper = new UserDbEntityRowMapper();

        UserDbEntity userDbEntity = namedParameterJdbcTemplate.queryForObject
                (SQLQueries.INSERT_REGISTER_USER, parameterSource, mapper);

        return userDbEntity;
    }

    private UserDbEntity selectUserWithUserName(String username) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(SQLQueries.PARAM_USERNAME, username);
        UserDbEntityRowMapper mapper = new UserDbEntityRowMapper();

        UserDbEntity userDbEntity = namedParameterJdbcTemplate.queryForObject
                (SQLQueries.SELECT_USER_WITH_USERNAME, parameterSource, mapper);

        return userDbEntity;
    }
}
