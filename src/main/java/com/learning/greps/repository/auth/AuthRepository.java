package com.learning.greps.repository.auth;

import com.learning.greps.domain.request.UserBody;
import com.learning.greps.domain.response.AuthResponseEntity;
import rx.Observable;

/**
 * Created by kamil on 20/06/2017.
 */

public interface AuthRepository {

    String JDBC_POSTGRES = "JDBC_POSTGRES";

    Observable<AuthResponseEntity> registerUser(UserBody userBody);

    Observable<AuthResponseEntity> loginUser(String username, String password);
}
