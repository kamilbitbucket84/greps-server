package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.response.AuthResponseEntity;
import com.learning.greps.repository.auth.AuthRepository;
import rx.Observable;

/**
 * Created by kamil on 30/05/2017.
 */

public class GetLoginUserUseCase implements UseCase<AuthResponseEntity> {

    private final AuthRepository authRepository;
    private final String username;
    private final String password;

    public GetLoginUserUseCase(AuthRepository authRepository) {
        this(authRepository, null, null);
    }

    private GetLoginUserUseCase(AuthRepository authRepository, String username, String password) {
        this.authRepository = authRepository;
        this.username = username;
        this.password = password;
    }

    public GetLoginUserUseCase withCredentials(String username, String password) {
        return new GetLoginUserUseCase(authRepository, username, password);
    }

    @Override
    public Observable<AuthResponseEntity> execute() {
        return authRepository.loginUser(username, password);
    }
}
