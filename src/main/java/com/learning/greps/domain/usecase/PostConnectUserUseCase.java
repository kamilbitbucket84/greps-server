package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.database.ConnectUserDbEntity;
import com.learning.greps.domain.request.ConnectionBody;
import com.learning.greps.domain.response.PostConnectUserResponseEntity;
import com.learning.greps.repository.user.UserRepository;
import rx.Observable;

/**
 * Created by kamil on 30/05/2017.
 */

public class PostConnectUserUseCase implements UseCase<PostConnectUserResponseEntity> {

    private final UserRepository userRepository;
    private final ConnectionBody connectionBody;

    public PostConnectUserUseCase(UserRepository userRepository) {
        this(userRepository, null);
    }

    private PostConnectUserUseCase(UserRepository userRepository, ConnectionBody connectionBody) {
        this.userRepository = userRepository;
        this.connectionBody = connectionBody;
    }

    public PostConnectUserUseCase withConnectionBody(ConnectionBody connectionBody) {
        return new PostConnectUserUseCase(userRepository, connectionBody);
    }

    @Override
    public Observable<PostConnectUserResponseEntity> execute() {
        return userRepository.connectUser(connectionBody).map(this::convert);
    }

    private PostConnectUserResponseEntity convert(ConnectUserDbEntity connectUserDbEntity) {
        return new PostConnectUserResponseEntity(connectUserDbEntity.idSender, connectUserDbEntity.idReceiver);
    }
}
