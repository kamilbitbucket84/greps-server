package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.request.UserBody;
import com.learning.greps.domain.response.AuthResponseEntity;
import com.learning.greps.repository.auth.AuthRepository;
import rx.Observable;

/**
 * Created by kamil on 30/05/2017.
 */

public class PostRegisterUserUseCase implements UseCase<AuthResponseEntity> {

    private final AuthRepository authRepository;
    private final UserBody userBody;

    public PostRegisterUserUseCase(AuthRepository authRepository) {
        this(authRepository, null);
    }

    private PostRegisterUserUseCase(AuthRepository authRepository, UserBody userBody) {
        this.authRepository = authRepository;
        this.userBody = userBody;
    }

    public PostRegisterUserUseCase withUserBody(UserBody userBody) {
        return new PostRegisterUserUseCase(authRepository, userBody);
    }

    @Override
    public Observable<AuthResponseEntity> execute() {
        return authRepository.registerUser(userBody);
    }
}
