package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.database.ConnectUserDbEntity;
import com.learning.greps.domain.request.ConnectionBody;
import com.learning.greps.domain.response.PostConnectUserResponseEntity;
import com.learning.greps.domain.response.PostDisconnectUserResponseEntity;
import com.learning.greps.repository.user.UserRepository;
import rx.Observable;

/**
 * Created by kamil on 06/06/2017.
 */

public class PostDisconnectUserUseCase implements UseCase<PostDisconnectUserResponseEntity> {

    private final UserRepository userRepository;
    private final ConnectionBody connectionBody;

    public PostDisconnectUserUseCase(UserRepository userRepository) {
        this(userRepository, null);
    }

    private PostDisconnectUserUseCase(UserRepository userRepository, ConnectionBody connectionBody) {
        this.userRepository = userRepository;
        this.connectionBody = connectionBody;
    }

    public PostDisconnectUserUseCase withConnectionBody(ConnectionBody connectionBody) {
        return new PostDisconnectUserUseCase(userRepository, connectionBody);
    }

    @Override
    public Observable<PostDisconnectUserResponseEntity> execute() {
        return userRepository.disconnectUser(connectionBody).map(this::convert);
    }

    private PostDisconnectUserResponseEntity convert(ConnectUserDbEntity connectUserDbEntity) {
        return new PostDisconnectUserResponseEntity(connectUserDbEntity.idSender, connectUserDbEntity.idReceiver);
    }
}
