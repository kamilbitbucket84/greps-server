package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.database.MessageDbEntity;
import com.learning.greps.domain.request.MessageBody;
import com.learning.greps.domain.response.SendMessageResponseEntity;
import com.learning.greps.repository.message.MessageRepository;
import rx.Observable;

/**
 * Created by kamil on 30/05/2017.
 */

public class PostSendMessageUseCase implements UseCase<SendMessageResponseEntity> {

    private final MessageRepository messageRepository;
    private final MessageBody messageBody;

    public PostSendMessageUseCase(MessageRepository messageRepository) {
        this(messageRepository, null);
    }

    private PostSendMessageUseCase(MessageRepository messageRepository, MessageBody messageBody) {
        this.messageRepository = messageRepository;
        this.messageBody = messageBody;
    }

    public PostSendMessageUseCase withMessageBody(MessageBody messageBody) {
        return new PostSendMessageUseCase(messageRepository, messageBody);
    }

    @Override
    public Observable<SendMessageResponseEntity> execute() {
        return messageRepository.sendMessage(messageBody).map(this::covert);
    }

    private SendMessageResponseEntity covert(MessageDbEntity messageDbEntity) {
        return new SendMessageResponseEntity.Builder()
                .messageId(messageDbEntity.idMessage)
                .senderId(messageDbEntity.idMessageSender)
                .receiverId(messageDbEntity.idMessageReceiver)
                .timeStamp(messageDbEntity.timeStamp)
                .build();
    }
}
