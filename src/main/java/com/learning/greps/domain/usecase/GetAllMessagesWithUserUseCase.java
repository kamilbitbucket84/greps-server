package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.database.MessageDbEntity;
import com.learning.greps.domain.mapper.GetMessagesWithUserResponseEntityMapper;
import com.learning.greps.domain.response.GetMessagesWithUserResponseEntity;
import com.learning.greps.repository.message.MessageRepository;
import rx.Observable;

import java.util.List;

/**
 * Created by kamil on 30/05/2017.
 */

public class GetAllMessagesWithUserUseCase implements UseCase<GetMessagesWithUserResponseEntity> {

    private final MessageRepository messageRepository;
    private final Long myId;
    private final Long contactId;

    public GetAllMessagesWithUserUseCase(MessageRepository messageRepository) {
        this(messageRepository, null, null);
    }

    private GetAllMessagesWithUserUseCase(MessageRepository messageRepository, Long myId, Long contactId) {
        this.messageRepository = messageRepository;
        this.myId = myId;
        this.contactId = contactId;
    }

    public GetAllMessagesWithUserUseCase withUsersId(Long myId, Long contactId) {
        return new GetAllMessagesWithUserUseCase(messageRepository, myId, contactId);
    }

    @Override
    public Observable<GetMessagesWithUserResponseEntity> execute() {
        return messageRepository.getAllMessagesWithUser(myId, contactId).map(this::convert);
    }

    private GetMessagesWithUserResponseEntity convert(List<MessageDbEntity> messageDbEntities) {
        return new GetMessagesWithUserResponseEntityMapper().from(messageDbEntities);
    }

}
