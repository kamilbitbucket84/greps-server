package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.database.FoundUserDbEntity;
import com.learning.greps.domain.mapper.GetFoundUsersResponseEntityMapper;
import com.learning.greps.domain.response.GetFoundUsersResponseEntity;
import com.learning.greps.repository.user.UserRepository;
import rx.Observable;

import java.util.List;

/**
 * Created by kamil on 30/05/2017.
 */

public class GetFoundUsersUseCase implements UseCase<GetFoundUsersResponseEntity> {

    private final UserRepository userRepository;
    private final String query;
    private final Long userId;

    public GetFoundUsersUseCase(UserRepository userRepository) {
        this(userRepository, null, null);
    }

    public GetFoundUsersUseCase(UserRepository userRepository, String query, Long userId) {
        this.userRepository = userRepository;
        this.query = query;
        this.userId = userId;
    }

    public GetFoundUsersUseCase withQuery(String userQuery, Long userId) {
        return new GetFoundUsersUseCase(userRepository, userQuery, userId);
    }

    @Override
    public Observable<GetFoundUsersResponseEntity> execute() {
        return userRepository.findUsers(query, userId).map(this::convert);
    }

    private GetFoundUsersResponseEntity convert(List<FoundUserDbEntity> userDbEntities) {
        return new GetFoundUsersResponseEntityMapper().from(userDbEntities);
    }
}
