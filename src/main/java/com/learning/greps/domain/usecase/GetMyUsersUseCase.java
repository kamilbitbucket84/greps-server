package com.learning.greps.domain.usecase;

import com.learning.greps.common.UseCase;
import com.learning.greps.domain.database.MyUserDbEntity;
import com.learning.greps.domain.mapper.GetMyUsersResponseEntityMapper;
import com.learning.greps.domain.response.GetMyUsersResponseEntity;
import com.learning.greps.repository.user.UserRepository;
import rx.Observable;

import java.util.List;

/**
 * Created by kamil on 30/05/2017.
 */
public class GetMyUsersUseCase implements UseCase<GetMyUsersResponseEntity> {

    private final UserRepository userRepository;
    private final Long userId;

    public GetMyUsersUseCase(UserRepository userRepository) {
        this(userRepository, null);
    }

    private GetMyUsersUseCase(UserRepository userRepository, Long userId) {
        this.userRepository = userRepository;
        this.userId = userId;
    }

    public GetMyUsersUseCase withUserId(Long userId) {
        return new GetMyUsersUseCase(userRepository, userId);
    }

    @Override
    public Observable<GetMyUsersResponseEntity> execute() {
        return userRepository.getMyUsers(userId).map(this::convert);
    }

    private GetMyUsersResponseEntity convert(List<MyUserDbEntity> userDbEntities) {
        return new GetMyUsersResponseEntityMapper().from(userDbEntities);
    }
}
