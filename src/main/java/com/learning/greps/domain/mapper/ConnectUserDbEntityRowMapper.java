package com.learning.greps.domain.mapper;

import com.learning.greps.domain.database.ConnectUserDbEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by kamil on 08/06/2017.
 */

public class ConnectUserDbEntityRowMapper implements RowMapper<ConnectUserDbEntity> {

    @Override
    public ConnectUserDbEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        ConnectUserDbEntity connectUserDbEntity = new ConnectUserDbEntity();
        connectUserDbEntity.idConnection = resultSet.getLong(ConnectUserDbEntity.COLUMN_ID);
        connectUserDbEntity.idSender = resultSet.getLong(ConnectUserDbEntity.COLUMN_ID_SENDER);
        connectUserDbEntity.idReceiver = resultSet.getLong(ConnectUserDbEntity.COLUMN_ID_RECEIVER);
        return connectUserDbEntity;
    }
}
