package com.learning.greps.domain.mapper;

import com.learning.greps.common.Mapper;
import com.learning.greps.domain.database.MessageDbEntity;
import com.learning.greps.domain.response.GetMessagesWithUserResponseEntity;
import com.learning.greps.domain.response.MessageEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 30/05/2017.
 */

public class GetMessagesWithUserResponseEntityMapper
        implements Mapper<List<MessageDbEntity>, GetMessagesWithUserResponseEntity> {

    @Override
    public GetMessagesWithUserResponseEntity from(List<MessageDbEntity> from) {
        List<MessageEntity> messageEntities = new ArrayList<>();

        MessageEntity messageEntity;
        for (MessageDbEntity messageDbEntity : from) {
            messageEntity = new MessageEntity.Builder()
                    .idMessage(messageDbEntity.idMessage)
                    .message(messageDbEntity.message)
                    .idMessageReceiver(messageDbEntity.idMessageReceiver)
                    .idMessageSender(messageDbEntity.idMessageSender)
                    .timeStamp(messageDbEntity.timeStamp)
                    .build();

            messageEntities.add(messageEntity);
        }

        return new GetMessagesWithUserResponseEntity(messageEntities);
    }
}
