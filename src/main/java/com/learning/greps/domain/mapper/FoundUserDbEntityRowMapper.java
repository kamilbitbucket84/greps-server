package com.learning.greps.domain.mapper;

import com.learning.greps.domain.database.FoundUserDbEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by kamil on 07/06/2017.
 */

public class FoundUserDbEntityRowMapper implements RowMapper<FoundUserDbEntity> {

    @Override
    public FoundUserDbEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        FoundUserDbEntity foundUserDbEntity = new FoundUserDbEntity();
        foundUserDbEntity.id = resultSet.getLong(FoundUserDbEntity.COLUMN_ID);
        foundUserDbEntity.userName = resultSet.getString(FoundUserDbEntity.COLUMN_USERNAME);
        foundUserDbEntity.connected = resultSet.getBoolean(FoundUserDbEntity.COLUMN_CONNECTED);
        return foundUserDbEntity;
    }
}
