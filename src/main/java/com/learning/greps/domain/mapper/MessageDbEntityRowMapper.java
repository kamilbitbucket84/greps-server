package com.learning.greps.domain.mapper;

import com.learning.greps.domain.database.MessageDbEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by kamil on 31/05/2017.
 */

public class MessageDbEntityRowMapper implements RowMapper<MessageDbEntity> {

    @Override
    public MessageDbEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        MessageDbEntity messageDbEntity = new MessageDbEntity();
        messageDbEntity.idMessage = resultSet.getLong(MessageDbEntity.COLUMN_ID_MESSAGE);
        messageDbEntity.message = resultSet.getString(MessageDbEntity.COLUMN_MESSAGE);
        messageDbEntity.idMessageReceiver = resultSet.getLong(MessageDbEntity.COLUMN_ID_RECEIVER);
        messageDbEntity.idMessageSender = resultSet.getLong(MessageDbEntity.COLUMN_ID_SENDER);
        messageDbEntity.timeStamp = resultSet.getLong(MessageDbEntity.COLUMN_TIMESTAMP);
        return messageDbEntity;
    }
}
