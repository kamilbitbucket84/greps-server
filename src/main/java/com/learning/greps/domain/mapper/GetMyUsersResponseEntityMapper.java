package com.learning.greps.domain.mapper;

import com.learning.greps.common.Mapper;
import com.learning.greps.domain.database.MyUserDbEntity;
import com.learning.greps.domain.response.GetMyUsersResponseEntity;
import com.learning.greps.domain.response.MyUserEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 30/05/2017.
 */

public class GetMyUsersResponseEntityMapper implements Mapper<List<MyUserDbEntity>, GetMyUsersResponseEntity> {

    @Override
    public GetMyUsersResponseEntity from(List<MyUserDbEntity> from) {
        List<MyUserEntity> userEntities = new ArrayList<>();

        for (MyUserDbEntity myUserDbEntity : from) {
            userEntities.add(new MyUserEntity(myUserDbEntity.id, myUserDbEntity.userName));
        }

        return new GetMyUsersResponseEntity(userEntities);
    }
}
