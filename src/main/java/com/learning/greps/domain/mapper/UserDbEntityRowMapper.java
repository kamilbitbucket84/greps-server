package com.learning.greps.domain.mapper;

import com.learning.greps.domain.database.UserDbEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by kamil on 20/06/2017.
 */

public class UserDbEntityRowMapper implements RowMapper<UserDbEntity> {

    @Override
    public UserDbEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        UserDbEntity userDbEntity = new UserDbEntity();
        userDbEntity.id = resultSet.getLong(UserDbEntity.COLUMN_ID);
        userDbEntity.username = resultSet.getString(UserDbEntity.COLUMN_USERNAME);
        userDbEntity.email = resultSet.getString(UserDbEntity.COLUMN_EMAIL);
        userDbEntity.password = resultSet.getString(UserDbEntity.COLUMN_PASSWORD);
        return userDbEntity;
    }
}
