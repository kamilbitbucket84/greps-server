package com.learning.greps.domain.mapper;

import com.learning.greps.common.Mapper;
import com.learning.greps.domain.database.FoundUserDbEntity;
import com.learning.greps.domain.response.FoundUserEntity;
import com.learning.greps.domain.response.GetFoundUsersResponseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kamil on 30/05/2017.
 */

public class GetFoundUsersResponseEntityMapper implements Mapper<List<FoundUserDbEntity>, GetFoundUsersResponseEntity> {

    @Override
    public GetFoundUsersResponseEntity from(List<FoundUserDbEntity> from) {
        List<FoundUserEntity> foundUserEntities = new ArrayList<>();

        for (FoundUserDbEntity foundUserDbEntity : from) {
            foundUserEntities.add(new FoundUserEntity(
                    foundUserDbEntity.id,
                    foundUserDbEntity.userName,
                    foundUserDbEntity.connected));
        }

        return new GetFoundUsersResponseEntity(foundUserEntities);
    }
}
