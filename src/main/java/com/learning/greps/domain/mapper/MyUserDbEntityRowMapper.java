package com.learning.greps.domain.mapper;

import com.learning.greps.domain.database.MyUserDbEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by kamil on 31/05/2017.
 */

public class MyUserDbEntityRowMapper implements RowMapper<MyUserDbEntity> {

    @Override
    public MyUserDbEntity mapRow(ResultSet resultSet, int i) throws SQLException {
        MyUserDbEntity myUserDbEntity = new MyUserDbEntity();
        myUserDbEntity.id = resultSet.getLong(MyUserDbEntity.COLUMN_ID);
        myUserDbEntity.userName = resultSet.getString(MyUserDbEntity.COLUMN_USERNAME);
        return myUserDbEntity;
    }
}