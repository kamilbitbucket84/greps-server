package com.learning.greps.domain.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static com.learning.greps.domain.database.MessageDbEntity.TABLE_NAME;

/**
 * Created by kamil on 29/05/2017.
 */

@Entity(name = TABLE_NAME)
@Table(name = TABLE_NAME)
public class MessageDbEntity {

    public static final String TABLE_NAME = "greps.message";
    public static final String COLUMN_ID_MESSAGE = "id_message";
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_ID_SENDER = "id_message_sender";
    public static final String COLUMN_ID_RECEIVER = "id_message_receiver";
    public static final String COLUMN_TIMESTAMP = "message_timestamp";

    @Id @Column(name = COLUMN_ID_MESSAGE) public Long idMessage;
    @Column(name = COLUMN_MESSAGE) public String message;
    @Column(name = COLUMN_ID_SENDER) public Long idMessageSender;
    @Column(name = COLUMN_ID_RECEIVER) public Long idMessageReceiver;
    @Column(name = COLUMN_TIMESTAMP) public Long timeStamp;
}
