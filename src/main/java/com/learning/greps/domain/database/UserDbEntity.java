package com.learning.greps.domain.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by kamil on 30/05/2017.
 */

@Entity
public class UserDbEntity {

    public static final String COLUMN_ID = "id_user";
    public static final String COLUMN_USERNAME = "user_name";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PASSWORD = "password";

    @Id @Column(name = COLUMN_ID) public long id;
    @Column(name = COLUMN_USERNAME) public String username;
    @Column(name = COLUMN_EMAIL) public String email;
    @Column(name = COLUMN_PASSWORD) public String password;
}
