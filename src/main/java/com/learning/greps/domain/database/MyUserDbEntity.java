package com.learning.greps.domain.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by kamil on 06/06/2017.
 */

@Entity
public class MyUserDbEntity {

    public static final String COLUMN_ID = "id_user";
    public static final String COLUMN_USERNAME = "user_name";

    @Id @Column(name = COLUMN_ID) public Long id;
    @Column(name = COLUMN_USERNAME) public String userName;
}
