package com.learning.greps.domain.database;

import javax.persistence.*;

import static com.learning.greps.domain.database.ConnectUserDbEntity.TABLE_NAME;

/**
 * Created by kamil on 05/06/2017.
 */

@Entity(name = TABLE_NAME)
@Table(name = TABLE_NAME)
public class ConnectUserDbEntity {

    public static final String TABLE_NAME = "greps.connected_users";
    public static final String COLUMN_ID = "id_connection";
    public static final String COLUMN_ID_SENDER = "id_sender";
    public static final String COLUMN_ID_RECEIVER = "id_receiver";

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = COLUMN_ID) public Long idConnection;
    @Column(name = COLUMN_ID_SENDER) public Long idSender;
    @Column(name = COLUMN_ID_RECEIVER) public Long idReceiver;
}