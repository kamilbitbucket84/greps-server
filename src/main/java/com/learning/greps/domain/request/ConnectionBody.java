package com.learning.greps.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 24/05/2017.
 */

public class ConnectionBody {

    @JsonProperty("sender_id") private Long senderId;
    @JsonProperty("receiver_id") private Long receiverId;

    public ConnectionBody(Long senderId, Long receiverId) {
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public ConnectionBody() {
    }

    public Long getSenderId() {
        return senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }
}
