package com.learning.greps.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 24/05/2017.
 */

public class MessageBody {

    @JsonProperty("sender_id") private Long senderId;
    @JsonProperty("receiver_id") private Long receiverId;
    @JsonProperty("message") private String message;

    public MessageBody(Long senderId, Long receiverId, String message) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.message = message;
    }

    public MessageBody() {
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
