package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 24/05/2017.
 */

public class MyUserEntity {

    @JsonProperty("id") private final Long id;
    @JsonProperty("username") private final String name;

    public MyUserEntity(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
