package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 24/05/2017.
 */

public class AuthResponseEntity {

    @JsonProperty("id") private Long id;
    @JsonProperty("user_name") private String userName;
    @JsonProperty("email") private String email;
    @JsonProperty("access_token") private String accessToken;

    public AuthResponseEntity(Long id, String userName, String email, String accessToken) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.accessToken = accessToken;
    }

    public AuthResponseEntity() {
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getEmail() {
        return email;
    }
}
