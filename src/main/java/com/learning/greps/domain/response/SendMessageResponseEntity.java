package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 25/05/2017.
 */

public class SendMessageResponseEntity {

    @JsonProperty("id_message") public Long messageId;
    @JsonProperty("sender_id") public Long senderId;
    @JsonProperty("receiver_id") public Long receiverId;
    @JsonProperty("timestamp") public Long timeStamp;

    public SendMessageResponseEntity() {

    }

    private SendMessageResponseEntity(Builder builder) {
        messageId = builder.messageId;
        senderId = builder.senderId;
        receiverId = builder.receiverId;
        timeStamp = builder.timeStamp;
    }

    public static final class Builder {
        private Long messageId;
        private Long senderId;
        private Long receiverId;
        private Long timeStamp;

        public Builder() {
        }

        public Builder messageId(Long messageId) {
            this.messageId = messageId;
            return this;
        }

        public Builder senderId(Long senderId) {
            this.senderId = senderId;
            return this;
        }

        public Builder receiverId(Long receiverId) {
            this.receiverId = receiverId;
            return this;
        }

        public Builder timeStamp(Long timeStamp) {
            this.timeStamp = timeStamp;
            return this;
        }

        public SendMessageResponseEntity build() {
            return new SendMessageResponseEntity(this);
        }
    }

    @Override
    public String toString() {
        return "SendMessageResponseEntity{" +
                "messageId=" + messageId +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
