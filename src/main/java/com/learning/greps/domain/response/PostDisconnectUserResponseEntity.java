package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 06/06/2017.
 */

public class PostDisconnectUserResponseEntity {

    @JsonProperty("sender_id") private final Long senderId;
    @JsonProperty("receiver_id") private final Long receiverId;

    public PostDisconnectUserResponseEntity(Long senderId, Long receiverId) {
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }
}
