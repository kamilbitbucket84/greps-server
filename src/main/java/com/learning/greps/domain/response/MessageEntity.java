package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 25/05/2017.
 */

public class MessageEntity {

    @JsonProperty("id_message") public final Long idMessage;
    @JsonProperty("message") public final String message;
    @JsonProperty("id_message_sender") public final Long idMessageSender;
    @JsonProperty("id_message_receiver") public final Long idMessageReceiver;
    @JsonProperty("timestamp") public final Long timeStamp;

    private MessageEntity(Builder builder) {
        idMessage = builder.idMessage;
        message = builder.message;
        idMessageSender = builder.idMessageSender;
        idMessageReceiver = builder.idMessageReceiver;
        timeStamp = builder.timeStamp;
    }

    public static final class Builder {
        private Long idMessage;
        private String message;
        private Long idMessageSender;
        private Long idMessageReceiver;
        private Long timeStamp;

        public Builder() {
        }

        public Builder idMessage(Long idMessage) {
            this.idMessage = idMessage;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder idMessageSender(Long idMessageSender) {
            this.idMessageSender = idMessageSender;
            return this;
        }

        public Builder idMessageReceiver(Long idMessageReceiver) {
            this.idMessageReceiver = idMessageReceiver;
            return this;
        }

        public Builder timeStamp(Long timeStamp) {
            this.timeStamp = timeStamp;
            return this;
        }

        public MessageEntity build() {
            return new MessageEntity(this);
        }
    }
}
