package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by kamil on 24/05/2017.
 */

public class GetMyUsersResponseEntity {

    @JsonProperty("my_users") private final List<MyUserEntity> addedUsers;

    public GetMyUsersResponseEntity(List<MyUserEntity> addedUsers) {
        this.addedUsers = addedUsers;
    }

    public List<MyUserEntity> getAddedUsers() {
        return addedUsers;
    }
}
