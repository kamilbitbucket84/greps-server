package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by kamil on 24/05/2017.
 */

public class GetFoundUsersResponseEntity {

    @JsonProperty("found_users") private final List<FoundUserEntity> foundUsers;

    public GetFoundUsersResponseEntity(List<FoundUserEntity> foundUsers) {
        this.foundUsers = foundUsers;
    }
}
