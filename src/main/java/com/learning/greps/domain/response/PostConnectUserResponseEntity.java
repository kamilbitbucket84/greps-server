package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 24/05/2017.
 */

public class PostConnectUserResponseEntity {

    @JsonProperty("sender_id") private final Long senderId;
    @JsonProperty("receiver_id") private final Long receiverId;

    public PostConnectUserResponseEntity(Long senderId, Long receiverId) {
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }
}
