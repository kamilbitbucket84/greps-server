package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kamil on 06/06/2017.
 */

public class FoundUserEntity {

    @JsonProperty("id") private final Long id;
    @JsonProperty("username") private final String username;
    @JsonProperty("connected") private final Boolean connected;

    public FoundUserEntity(Long id, String username, Boolean connected) {
        this.id = id;
        this.username = username;
        this.connected = connected;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Boolean getConnected() {
        return connected;
    }
}
