package com.learning.greps.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by kamil on 25/05/2017.
 */

public class GetMessagesWithUserResponseEntity {

    @JsonProperty("messages_with_user") private final List<MessageEntity> messagesWithUser;

    public GetMessagesWithUserResponseEntity(List<MessageEntity> messagesWithUser) {
        this.messagesWithUser = messagesWithUser;
    }

    public List<MessageEntity> getMessagesWithUser() {
        return messagesWithUser;
    }
}
