package com.learning.greps.config;

import com.learning.greps.domain.usecase.*;
import com.learning.greps.repository.auth.AuthRepository;
import com.learning.greps.repository.message.MessageRepository;
import com.learning.greps.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by kamil on 30/05/2017.
 */

@Configuration
public class UseCaseConfig {

    @Autowired @Qualifier(UserRepository.JDBC_POSTGRES) UserRepository userRepository;
    @Autowired @Qualifier(MessageRepository.JDBC_POSTGRES) MessageRepository messageRepository;
    @Autowired @Qualifier(AuthRepository.JDBC_POSTGRES) AuthRepository authRepository;

    @Bean
    GetLoginUserUseCase loginUserUseCase() {
        return new GetLoginUserUseCase(authRepository);
    }

    @Bean
    PostRegisterUserUseCase postRegisterUserUseCase() {
        return new PostRegisterUserUseCase(authRepository);
    }

    @Bean
    GetFoundUsersUseCase getFoundUsersUsecase() {
        return new GetFoundUsersUseCase(userRepository);
    }

    @Bean
    GetMyUsersUseCase getMyUsersUseCase() {
        return new GetMyUsersUseCase(userRepository);
    }

    @Bean
    PostConnectUserUseCase postConnectUserUseCase() {
        return new PostConnectUserUseCase(userRepository);
    }

    @Bean
    PostDisconnectUserUseCase postDisconnectUserUseCase() {
        return new PostDisconnectUserUseCase(userRepository);
    }

    @Bean
    GetAllMessagesWithUserUseCase getAllMessagesWithUserUseCase() {
        return new GetAllMessagesWithUserUseCase(messageRepository);
    }

    @Bean
    PostSendMessageUseCase postSendMessageUseCase() {
        return new PostSendMessageUseCase(messageRepository);
    }
}
