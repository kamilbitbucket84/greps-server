package com.learning.greps.config;

import com.learning.greps.service.auth.Authenticator;
import com.learning.greps.service.auth.GrepsAuthenticator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by kamil on 19/06/2017.
 */

@Configuration
@PropertySource("credentials.properties")
public class SecurityConfig {

    @Value("${password.salt}") private String passwordSalt;
    @Value("${token.signkey}") private String signKey;
    @Value("${signkey.encoding}") private String encoding;
    @Value("${token.issuer}") private String issuer;
    @Value("${token.usage.expiration}") private Long usageExpiration;
    @Value("${token.creation.expiration}") private Long creationExpiration;

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    Authenticator authenticator(BCryptPasswordEncoder bCryptPasswordEncoder) {
        return new GrepsAuthenticator.Builder()
                .encoder(bCryptPasswordEncoder)
                .passwordSalt(passwordSalt)
                .signKey(signKey)
                .encoding(encoding)
                .issuer(issuer)
                .tokenUsageExpirationTime(usageExpiration)
                .tokenCreationExpirationTime(creationExpiration)
                .build();
    }
}
