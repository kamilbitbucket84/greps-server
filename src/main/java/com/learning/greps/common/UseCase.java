package com.learning.greps.common;

import rx.Observable;

public interface UseCase<T> {
    Observable<T> execute();
}
