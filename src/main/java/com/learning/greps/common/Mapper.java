package com.learning.greps.common;

public interface Mapper<T,V> {
    V from(T from);
}
