package com.learning.greps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.SpringServletContainerInitializer;

@SpringBootApplication
public class GrepsApplication extends SpringServletContainerInitializer {

    public static void main(String[] args) {
        SpringApplication.run(GrepsApplication.class, args);
    }
}
