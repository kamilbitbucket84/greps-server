package com.learning.greps.controller;

import rx.Scheduler;
import rx.schedulers.Schedulers;

import java.util.concurrent.Executors;

/**
 * Created by kamil on 01/06/2017.
 */
public enum RequestScheduler {

    SCHEDULER(Schedulers.from(Executors.newFixedThreadPool(128)));

    public final Scheduler scheduler;

    RequestScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
}
