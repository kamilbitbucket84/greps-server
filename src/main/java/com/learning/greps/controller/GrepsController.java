package com.learning.greps.controller;

import com.learning.greps.domain.request.ConnectionBody;
import com.learning.greps.domain.request.MessageBody;
import com.learning.greps.domain.request.UserBody;
import com.learning.greps.domain.response.*;
import com.learning.greps.service.auth.AuthService;
import com.learning.greps.service.auth.Authenticator;
import com.learning.greps.service.auth.GrepsAuthenticator;
import com.learning.greps.service.greps.GrepsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;
import rx.functions.Action1;

/**
 * Created by kamil on 24/05/2017.
 */

@RestController
@RequestMapping("/greps")
public class GrepsController {

    private static final long REQUEST_TIMEOUT = 30000;

    @Autowired private GrepsService grepsService;
    @Autowired private AuthService authService;
    @Autowired private Authenticator grepsAuthenticator;

    private static class Params {
        private static final String HEADER_USERNAME = "username";
        private static final String HEADER_PASSWORD = "password";
        private static final String HEADER_AUTHORIZATION = "Authorization";
        private static final String PARAM_QUERY = "query";
        private static final String PARAM_USER_ID = "user_id";
        private static final String PATH_USER_ID = "id";
        private static final String PARAM_SENDER_ID = "sender_id";
        private static final String PARAM_RECEIVER_ID = "receiver_id";
    }

    @RequestMapping(
            value = "/users/login",
            method = RequestMethod.GET)
    public DeferredResult<AuthResponseEntity> loginUser(@RequestHeader(Params.HEADER_USERNAME) String username,
                                                        @RequestHeader(Params.HEADER_PASSWORD) String password) {
        DeferredResult<AuthResponseEntity> loginResult = new DeferredResult<>(REQUEST_TIMEOUT);
        authService.loginUser(username, password)
                .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                .subscribe(loginResult::setResult, loginResult::setErrorResult);

        return loginResult;
    }

    @RequestMapping(
            value = "/users/register",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<AuthResponseEntity> registerUser(@RequestBody UserBody userBody) {
        DeferredResult<AuthResponseEntity> registerResult = new DeferredResult<>(REQUEST_TIMEOUT);
        authService.registerUser(userBody)
                .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                .subscribe(registerResult::setResult, registerResult::setErrorResult);

        return registerResult;
    }

    @RequestMapping(
            value = "/users/myusers/{id}",
            method = RequestMethod.GET)
    public DeferredResult<GetMyUsersResponseEntity> getMyUsers(@RequestHeader(Params.HEADER_AUTHORIZATION) String accessToken,
                                                               @PathVariable(Params.PATH_USER_ID) Long userId) {
        DeferredResult<GetMyUsersResponseEntity> getMyUsersResult = new DeferredResult<>(REQUEST_TIMEOUT);
        grepsService.getMyUsers(userId)
                .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                .subscribe(getMyUsersResult::setResult, getMyUsersResult::setErrorResult);

        return getMyUsersResult;
    }

    @RequestMapping(
            value = "/users",
            method = RequestMethod.GET)
    public DeferredResult<GetFoundUsersResponseEntity> foundUsers(@RequestHeader(Params.HEADER_AUTHORIZATION) String accessToken,
                                                                  @RequestParam(Params.PARAM_QUERY) String query,
                                                                  @RequestParam(Params.PARAM_USER_ID) Long userId) {
        DeferredResult<GetFoundUsersResponseEntity> foundUsersResult = new DeferredResult<>(REQUEST_TIMEOUT);
        grepsService.findUsers(query, userId)
                .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                .subscribe(foundUsersResult::setResult, foundUsersResult::setErrorResult);

        return foundUsersResult;
    }

    @RequestMapping(
            value = "/users/connect",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<PostConnectUserResponseEntity> connectUser(@RequestHeader(Params.HEADER_AUTHORIZATION) String accessToken,
                                                                     @RequestBody ConnectionBody connectionBody) {
        DeferredResult<PostConnectUserResponseEntity> postAddUser = new DeferredResult<>(REQUEST_TIMEOUT);
        grepsService.connectUser(connectionBody)
                .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                .subscribe(postAddUser::setResult, postAddUser::setErrorResult);

        return postAddUser;
    }

    @RequestMapping(
            value = "/users/disconnect",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<PostDisconnectUserResponseEntity> disconnectUser(@RequestHeader(Params.HEADER_AUTHORIZATION) String accessToken,
                                                                           @RequestBody ConnectionBody disconnectionBody) {
        DeferredResult<PostDisconnectUserResponseEntity> postAddUser = new DeferredResult<>(REQUEST_TIMEOUT);
        grepsService.disconnectUser(disconnectionBody)
                .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                .subscribe(postAddUser::setResult, postAddUser::setErrorResult);

        return postAddUser;
    }

    @RequestMapping(
            value = "/messages",
            method = RequestMethod.GET)
    public DeferredResult<GetMessagesWithUserResponseEntity> getMessagesWithUser(@RequestHeader(Params.HEADER_AUTHORIZATION) String accessToken,
                                                                                 @RequestParam(Params.PARAM_SENDER_ID) Long myId,
                                                                                 @RequestParam(Params.PARAM_RECEIVER_ID) Long contactId) {
        DeferredResult<GetMessagesWithUserResponseEntity> getMessagesWithUserResult = new DeferredResult<>(REQUEST_TIMEOUT);

        grepsService.getMessagesWithUser(myId, contactId)
                .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                .subscribe(getMessagesWithUserResult::setResult, getMessagesWithUserResult::setErrorResult);

        return getMessagesWithUserResult;
    }

    @RequestMapping(
            value = "/messages/send",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public DeferredResult<SendMessageResponseEntity> sendMessageToUser(@RequestHeader(Params.HEADER_AUTHORIZATION) String accessToken,
                                                                       @RequestBody MessageBody messageBody) {
        DeferredResult<SendMessageResponseEntity> sendMessageToUserResult = new DeferredResult<>(REQUEST_TIMEOUT);
        if (grepsAuthenticator.isTokenValid(accessToken)) {
            grepsService.sendMessage(messageBody)
                    .subscribeOn(RequestScheduler.SCHEDULER.scheduler)
                    .subscribe(sendMessageToUserResult::setResult, sendMessageToUserResult::setErrorResult);
        } else {
            sendMessageToUserResult.setErrorResult("not authenticated");
        }

        return sendMessageToUserResult;
    }

}
