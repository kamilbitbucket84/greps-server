DROP TABLE IF EXISTS greps.message;
DROP TABLE IF EXISTS greps.connected_users;
DROP TABLE IF EXISTS greps.greps_user;


CREATE TABLE greps.greps_user (
	id_user BIGSERIAL NOT NULL PRIMARY KEY,
	user_name varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	password varchar NOT null,
	constraint username_un UNIQUE(user_name)
)
WITH (
	OIDS=FALSE
);

CREATE TABLE greps.connected_users (
	id_connection BIGSERIAL NOT NULL,
	id_sender int8 NOT NULL,
	id_receiver int8 NOT NULL,
	CONSTRAINT connected_users_pk PRIMARY KEY (id_sender,id_receiver),
	CONSTRAINT connected_users_un UNIQUE (id_sender,id_receiver),
	CONSTRAINT connected_users_check CHECK (id_sender <> id_receiver),
	CONSTRAINT id_user_id_receiver_fk FOREIGN KEY (id_receiver) REFERENCES greps.greps_user(id_user) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT id_user_id_sender_fk FOREIGN KEY (id_sender) REFERENCES greps.greps_user(id_user) ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (
	OIDS=FALSE
) ;


CREATE TABLE greps.message (
	id_message BIGSERIAL NOT NULL ,
	id_message_sender int8 NOT NULL,
	id_message_receiver int8 NOT NULL,
	message varchar NOT NULL,
	message_timestamp int8 NOT NULL DEFAULT (extract(epoch from now()) * 1000),
	CONSTRAINT message_connected_users_fk FOREIGN KEY (id_message_sender,id_message_receiver)
		REFERENCES greps.connected_users(id_sender,id_receiver)
			ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (
	OIDS=FALSE
) ;

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------


insert into greps.greps_user (id_user,user_name, password, email) values (1,'Ossie', 'uCkb7DbBp', 'ossie@mail.com');
insert into greps.greps_user (id_user,user_name, password, email) values (2,'Barr', 'gQoQWIz', 'barr@mail.com');
insert into greps.greps_user (id_user,user_name, password, email) values (3,'Jorie', 'Fy50UqTI', 'jorie@mail.com');
insert into greps.greps_user (id_user,user_name, password, email) values (4,'Chaim', 'OFcrEgHAaOC', 'chaim@mail.com');
insert into greps.greps_user (id_user,user_name, password, email) values (5,'Launce', 'kU45mZ', 'launce@mail.com');


----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

insert into greps.connected_users(id_sender, id_receiver) values(1,2);
insert into greps.connected_users(id_sender, id_receiver) values(2,1);
insert into greps.connected_users(id_sender, id_receiver) values(1,3);
insert into greps.connected_users(id_sender, id_receiver) values(3,1);
insert into greps.connected_users(id_sender, id_receiver) values(1,4);
insert into greps.connected_users(id_sender, id_receiver) values(4,1);

----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------

insert into greps.message(id_message_sender, id_message_receiver, message) values(1,2, 'Siemanko');
insert into greps.message(id_message_sender, id_message_receiver, message) values(2,1, 'a siema siema');
insert into greps.message(id_message_sender, id_message_receiver, message) values(1,2, 'jak zyjesz');
insert into greps.message(id_message_sender, id_message_receiver, message) values(2,1, 'a dziekuje dobrze a Ty?');
insert into greps.message(id_message_sender, id_message_receiver, message) values(1,2, 'tez dobrze');

