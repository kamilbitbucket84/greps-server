package com.learning.greps.api.common;

/**
 * Created by kamil on 23/06/2017.
 */
public final class StatusCodes {

    public static final int STATUS_BAD_REQUEST = 400;
    public static final int STATUS_UNAUTHORIZED_REQUEST = 401;
    public static final int STATUS_OK = 200;
    public static final int STATUS_UNSUPPORTED_MEDIA_TYPE = 415;
    public static final int STATUS_UNSUPPORTED_METHOD_TYPE = 405;

    private StatusCodes() {
    }
}
