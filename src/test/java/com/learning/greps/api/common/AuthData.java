package com.learning.greps.api.common;

/**
 * Created by kamil on 23/06/2017.
 */
public class AuthData {

    public final String accessToken;

    public AuthData(String accessToken) {
        this.accessToken = accessToken;
    }
}
