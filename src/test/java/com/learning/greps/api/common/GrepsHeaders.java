package com.learning.greps.api.common;

/**
 * Created by kamil on 23/06/2017.
 */
public final class GrepsHeaders {

    private GrepsHeaders() {
    }

    public static final String HEADER_KEY_AUTHORIZATION = "Authorization";
    public static final String HEADER_KEY_CONTENT_TYPE= "Content-Type";
    public static final String HEADER_USERNAME= "username";
    public static final String HEADER_PASSWORD= "password";
}
