package com.learning.greps.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learning.greps.api.common.AuthData;
import com.learning.greps.api.common.GrepsHeaders;
import com.learning.greps.api.common.StatusCodes;
import com.learning.greps.api.common.TestApiHeadersInterceptor;
import com.learning.greps.domain.request.MessageBody;
import com.learning.greps.domain.response.AuthResponseEntity;
import com.learning.greps.domain.response.SendMessageResponseEntity;
import io.restassured.response.Response;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.restassured.RestAssured.given;


/**
 * Created by kamil on 21/06/2017.
 */

public class SendMessageApiTest {

    private static final String URL = "http://localhost:8080/greps/messages/send";
    private static final String LOGIN_URL = "http://localhost:8080/greps/users/login";

    private ObjectMapper objectMapper;

    private AuthData authData;
    private Long goodSenderId = 1L;
    private Long goodReceiverId = 2L;
    private String goodMessage = "hey hello whatsup";

    @BeforeTest
    public void setUp() throws IOException {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        authData = getAuthData();
    }

    private AuthData getAuthData() throws IOException {
        Response response = given()
                .log().uri()
                .header(GrepsHeaders.HEADER_USERNAME, "franek")
                .header(GrepsHeaders.HEADER_PASSWORD, "siemanko")
                .get(LOGIN_URL)
                .then()
                .extract()
                .response();

        AuthResponseEntity authResponseEntity = objectMapper.readValue(response.asString(), AuthResponseEntity.class);
        return new AuthData(authResponseEntity.getAccessToken());
    }

    private SendMessageResponseEntity performValidRequest() throws IOException {
        Response response = given()
                .log().uri()
                .header(GrepsHeaders.HEADER_KEY_AUTHORIZATION, authData.accessToken)
                .header(GrepsHeaders.HEADER_KEY_CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(objectMapper.writeValueAsString(new MessageBody(goodSenderId, goodReceiverId, goodMessage)))
                .post(URL)
                .then()
                .statusCode(StatusCodes.STATUS_OK)
                .log()
                .status()
                .extract()
                .response();

        return objectMapper.readValue(response.asString(), SendMessageResponseEntity.class);
    }

    @Test
    public void sendMessage_validInput_statusOk() throws IOException {
        performValidRequest();
    }

    @Test
    public void sendMessage_validInput_responseNotNull() throws IOException {
        SendMessageResponseEntity sendMessageResponseEntity = performValidRequest();
        Assert.assertTrue(sendMessageResponseEntity != null);
    }

    @Test
    public void sendMessage_validInput_responseFieldsNotNull() throws IOException {
        SendMessageResponseEntity sendMessageResponseEntity = performValidRequest();
        Assert.assertTrue(sendMessageResponseEntity.messageId != null);
        Assert.assertTrue(sendMessageResponseEntity.senderId != null);
        Assert.assertTrue(sendMessageResponseEntity.receiverId != null);
        Assert.assertTrue(sendMessageResponseEntity.timeStamp != null);
    }

    @Test
    public void sendMessage_validInput_receiverAndSenderSameAsInInput() throws IOException {
        SendMessageResponseEntity sendMessageResponseEntity = performValidRequest();
        Assert.assertTrue(sendMessageResponseEntity.senderId.equals(goodSenderId));
        Assert.assertTrue(sendMessageResponseEntity.receiverId.equals(goodReceiverId));
    }

    @Test
    public void sendMessage_validInput_messageIdLargerThanZero() throws IOException {
        SendMessageResponseEntity sendMessageResponseEntity = performValidRequest();
        Assert.assertTrue(sendMessageResponseEntity.messageId > 0);
    }

    @Test
    public void sendMessage_validInput_timestampLargerThanZero() throws IOException {
        SendMessageResponseEntity sendMessageResponseEntity = performValidRequest();
        Assert.assertTrue(sendMessageResponseEntity.timeStamp > 0);
    }

    @Test
    public void sendMessage_withoutMediaType_statusUnsupportedMediaType() {
        given().post(URL)
                .then()
                .statusCode(StatusCodes.STATUS_UNSUPPORTED_MEDIA_TYPE);
    }

    @Test
    public void sendMessage_wrongMethod_statusUnsupportedMethodType() {
        given().get(URL)
                .then()
                .statusCode(StatusCodes.STATUS_UNSUPPORTED_METHOD_TYPE);
    }

    @Test
    public void sendMessage_withoutAuthorization_badRequest() {
        given().header(GrepsHeaders.HEADER_KEY_CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .post(URL)
                .then()
                .statusCode(StatusCodes.STATUS_BAD_REQUEST);
    }

}
