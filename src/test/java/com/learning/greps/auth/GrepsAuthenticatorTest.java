package com.learning.greps.auth;

import com.learning.greps.config.SecurityConfig;
import com.learning.greps.service.auth.Authenticator;
import com.learning.greps.service.auth.GrepsAuthenticator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by kamil on 30/06/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SecurityConfig.class)
public class GrepsAuthenticatorTest {

    @Autowired BCryptPasswordEncoder encoder;
    @Value("${password.salt}") private String passwordSalt;
    @Value("${token.issuer}") private String issuer;
    @Value("${token.signkey}") private String signKey;
    @Value("${signkey.encoding}") private String encoding;
    @Value("${token.usage.expiration}") private Long usageExpiration;
    @Value("${token.creation.expiration}") private Long creationExpiration;

    private Authenticator authenticator;

    private String goodToken = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHcmVwcyBjb21wYW55IiwiZXhwIjozMzAzNDgxNzYxNiwiaWQiOjgwLCJ1c2VyX25hbWUiOiJmcmFuZWsifQ.FnX_60s5F33Ytd1bGPn8LTIK4zMOZ8B1lzLSE1jcvWA";
    private String badMalformedToken = "yJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHcmVwcyBjb21wYW55IiwiZXhwIjozMzAzNDgxNzYxNiwiaWQiOjgwLCJ1c2VyX25hbWUiOiJmcmFuZWsifQ.FnX_60s5F33Ytd1bGPn8LTIK4zMOZ8B1lzLSE1jcvWA";
    private String badVeryShortToken = "sadsadsadsad";

    @Before
    public void setUp() {
        authenticator = new GrepsAuthenticator.Builder()
                .encoder(encoder)
                .passwordSalt(passwordSalt)
                .issuer(issuer)
                .encoding(encoding)
                .signKey(signKey)
                .tokenCreationExpirationTime(creationExpiration)
                .tokenUsageExpirationTime(usageExpiration)
                .build();
    }

    @Test
    public void isTokenValid_goodTokenAddedToCache_Valid() {
        authenticator.addTokenToCache(goodToken);
        assertTrue(authenticator.isTokenValid(goodToken));
    }

    @Test
    public void isTokenValid_goodTokenNotAddedToCache_Invalid() {
        assertTrue(!authenticator.isTokenValid(goodToken));
    }

    @Test
    public void isTokenValid_badVeryShortToken_Invalid() {
        assertTrue(!authenticator.isTokenValid(badVeryShortToken));
    }

    @Test
    public void isTokenValid_badMalformedToken_Invalid() {
        assertTrue(!authenticator.isTokenValid(badMalformedToken));
    }

    @Test
    public void isTokenValid_expirationUsageEnded_Invalid() throws Exception {
        authenticator = new GrepsAuthenticator.Builder()
                .encoder(encoder)
                .passwordSalt(passwordSalt)
                .issuer(issuer)
                .encoding(encoding)
                .signKey(signKey)
                .tokenCreationExpirationTime(creationExpiration)
                .tokenUsageExpirationTime(10)
                .build();

        authenticator.addTokenToCache(goodToken);
        Thread.sleep(50);

        Assert.assertTrue(!authenticator.isTokenValid(goodToken));
    }

    @Test
    public void isTokenValid_expirationUsageNotEnded_Valid() throws Exception {
        authenticator = new GrepsAuthenticator.Builder()
                .encoder(encoder)
                .passwordSalt(passwordSalt)
                .issuer(issuer)
                .encoding(encoding)
                .signKey(signKey)
                .tokenCreationExpirationTime(creationExpiration)
                .tokenUsageExpirationTime(50)
                .build();

        authenticator.addTokenToCache(goodToken);
        Thread.sleep(10);

        Assert.assertTrue(authenticator.isTokenValid(goodToken));
    }
}
